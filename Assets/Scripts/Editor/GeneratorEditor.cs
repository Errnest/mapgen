﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Generator))]
public class GeneratorEditor : Editor
{

    public override void OnInspectorGUI()
    {
        Generator gen = (Generator)target;


        DrawDefaultInspector();
        if (GUILayout.Button("Generate Tiles"))
        {
            gen.InitializeTiles();
        }
        if (GUILayout.Button("Draw Texture"))
        {
            gen.DrawTexture();
        }
        if(GUILayout.Button("Apply to terrain"))
        {
            gen.ApplyToTerrain();
        }
    }
}
