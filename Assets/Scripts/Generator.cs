﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AccidentalNoise;
using System.Linq;


public enum TypeOfScan
{
    TwoD, ThreeD, FourD
}

public enum TypeDraw
{
    NoiseMap, Topography, WaterLand
}

public class Generator : MonoBehaviour {

    [Header("Generator Values")]
    [SerializeField]
    TypeOfScan typeOfScan;
    [SerializeField]
    TypeDraw typeOfTexture;
    [SerializeField]
    Terrain terrain;

    // Adjustable variables for Unity Inspector
    [SerializeField]
    int Width = 256;
    [SerializeField]
    int Height = 256;

    [Header("Height Map")]
    [SerializeField]
    int TerrainOctaves = 6;
    [SerializeField]
    double TerrainFrequency = 1.25;
    [SerializeField]
    float DeepWater = 0.2f;
    [SerializeField]
    float ShallowWater = 0.4f;
    [SerializeField]
    float Sand = 0.5f;
    [SerializeField]
    float Grass = 0.7f;
    [SerializeField]
    float Forest = 0.8f;
    [SerializeField]
    float Rock = 0.9f;

    //[Header("Heat Map")]
    //[SerializeField]
    //float ColdestValue = 0.05f;
    //[SerializeField]
    //float ColderValue = 0.18f;
    //[SerializeField]
    //float ColdValue = 0.4f;
    //[SerializeField]
    //float WarmValue = 0.6f;
    //[SerializeField]
    //float WarmerValue = 0.8f;


    [Header("Rivers")]
    [SerializeField]
    int RiverCount = 40;
    [SerializeField]
    float MinRiverHeight = 0.6f;
    [SerializeField]
    int MaxRiverAttempts = 1000;
    [SerializeField]
    int MinRiverTurns = 18;
    [SerializeField]
    int MinRiverLength = 20;
    [SerializeField]
    int MaxRiverIntersections = 2;

    List<River> Rivers = new List<River>();
    List<RiverGroup> RiverGroups = new List<RiverGroup>();


    // Noise generator module
    ImplicitFractal HeightMap;

    // Height map data
    MapData HeightData;

    // Final Objects
    Tile[,] Tiles;

    // Our texture output (unity component)
    MeshRenderer HeightMapRenderer;

    public List<TileGroup> Waters;
    public List<TileGroup> Lands;


    bool IsCompleteInitialization = false;

    private void Start()
    {
        InitializeTiles();
        DrawTexture();
        ApplyToTerrain();
    }

    public void ApplyToTerrain()
    {
        terrain.terrainData.SetHeights(0, 0, TilesToArray());
    }  

    private float[,] TilesToArray()
    {
        float[,] map = new float[Width, Height];

        for(var x = 0; x < Width; x++)
        {
            for(var y = 0; y < Height; y++)
            {
                map[x, y] = Tiles[x, y].HeightValue / 10;
                if (Tiles[x, y].HeightType == HeightType.River)
                {
                    Tile tile = Tiles[x, y];
                    int counter = 5;
                    while (tile.HeightType == HeightType.River && counter > 0)
                    {
                        if(tile.Left != null)
                         tile = tile.Left;
                        counter--;
                    }
                    if (tile.HeightType == HeightType.River)
                        tile = Tiles[x, y];
                    while(tile.HeightType == HeightType.River)
                    {
                        if (tile.Top != null)
                            tile = tile.Top;
                    }
                    map[x, y] = (tile.HeightValue - Random.Range(0.02f, 0.04f)) / 10f;
                }
            }
        }

        return map;
    }


    public void DrawTexture()
    {
        if (IsCompleteInitialization)
        {
            // Get the mesh we are rendering our output to
            HeightMapRenderer = transform.Find("HeightTexture").GetComponent<MeshRenderer>();
            // Render a texture representation of our map
            //if (textureType == TypeOfTexture.LakesOceans || textureType == TypeOfTexture.LandmassIslands)
            //{
            //    HeightMapRenderer.sharedMaterial.mainTexture = TextureGenerator.GetTexture(Width, Height, Waters, Lands, textureType);
            //}
            //else
            //{
            //    HeightMapRenderer.sharedMaterial.mainTexture = TextureGenerator.GetTexture(Width, Height, Tiles, textureType);
            //}
            if (typeOfTexture == TypeDraw.NoiseMap)
            {
                HeightMapRenderer.sharedMaterial.mainTexture = TextureGenerator.GetNoiseMapTexture(Width, Height, Tiles);
            }
            else if (typeOfTexture == TypeDraw.Topography)
            {
                HeightMapRenderer.sharedMaterial.mainTexture = TextureGenerator.GetHeightMapTexture(Width, Height, Tiles);
            }
            else if(typeOfTexture == TypeDraw.WaterLand)
            {
                HeightMapRenderer.sharedMaterial.mainTexture = TextureGenerator.GetWaterLandMapTexture(Width, Height, Tiles);
            }
        }
    }


    public void InitializeTiles()
    {
        IsCompleteInitialization = false;

        

        // Initialize the generator
        Initialize();

        // Build the height map
        GetData(HeightMap, ref HeightData, typeOfScan);

        // Build our final objects based on our data
        LoadTiles();
        GenerateRivers();
        BuildRiverGroups();
        DigRiverGroups();

        Waters = new List<TileGroup>();
        Lands = new List<TileGroup>();
        FloodFill();

        IsCompleteInitialization = true;
      
    }

    private void Initialize()
    {
        // Initialize the HeightMap Generator
        HeightMap = new ImplicitFractal(FractalType.MULTI,
            BasisType.SIMPLEX,
            InterpolationType.QUINTIC,
            TerrainOctaves,
            TerrainFrequency,
            UnityEngine.Random.Range(0, int.MaxValue));
    }

    // Extract data from a noise module
    private void GetData(ImplicitModuleBase module, ref MapData mapData, TypeOfScan type = TypeOfScan.TwoD)
    {
        if (type == TypeOfScan.TwoD)
            GetData2d(ref mapData);
        else if (type == TypeOfScan.ThreeD)
            GetData3d(ref mapData);
        else if (type == TypeOfScan.FourD)
            GetData4D(ref mapData);        
    }

    private void GetData2d(ref MapData mapData)
    {
        mapData = new MapData(Width, Height);

        // loop through each x,y point - get height value
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                //Sample the noise at smaller intervals
                float x1 = x / (float)Width;
                float y1 = y / (float)Height;

                float value = (float)HeightMap.Get(x1, y1);

                //keep track of the max and min values found
                if (value > mapData.Max) mapData.Max = value;
                if (value < mapData.Min) mapData.Min = value;

                mapData.Data[x, y] = value;
            }
        }
    }


    private void GetData3d(ref MapData mapData)
    {
        mapData = new MapData(Width, Height);

        // loop through each x,y point - get height value
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {

                //Noise range
                float x1 = 0, x2 = 1;
                float y1 = 0, y2 = 1;
                float dx = x2 - x1;
                float dy = y2 - y1;

                //Sample noise at smaller intervals
                float s = x / (float)Width;
                float t = y / (float)Height;

                // Calculate our 3D coordinates
                float nx = x1 + Mathf.Cos(s * 2 * Mathf.PI) * dx / (2 * Mathf.PI);
                float ny = x1 + Mathf.Sin(s * 2 * Mathf.PI) * dx / (2 * Mathf.PI);
                float nz = t;

                float heightValue = (float)HeightMap.Get(nx, ny, nz);

                // keep track of the max and min values found
                if (heightValue > mapData.Max)
                    mapData.Max = heightValue;
                if (heightValue < mapData.Min)
                    mapData.Min = heightValue;

                mapData.Data[x, y] = heightValue;
            }
        }
    }


    private void GetData4D(ref MapData mapData)
    {
        mapData = new MapData(Width, Height);

        // loop through each x,y point - get height value
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                // Noise range
                float x1 = 0, x2 = 2;
                float y1 = 0, y2 = 2;
                float dx = x2 - x1;
                float dy = y2 - y1;

                // Sample noise at smaller intervals
                float s = x / (float)Width;
                float t = y / (float)Height;

                // Calculate our 4D coordinates
                float nx = x1 + Mathf.Cos(s * 2 * Mathf.PI) * dx / (2 * Mathf.PI);
                float ny = y1 + Mathf.Cos(t * 2 * Mathf.PI) * dy / (2 * Mathf.PI);
                float nz = x1 + Mathf.Sin(s * 2 * Mathf.PI) * dx / (2 * Mathf.PI);
                float nw = y1 + Mathf.Sin(t * 2 * Mathf.PI) * dy / (2 * Mathf.PI);


                float heightValue = (float)HeightMap.Get(nx, ny, nz, nw);

                // keep track of the max and min values found
                if (heightValue > mapData.Max) mapData.Max = heightValue;
                if (heightValue < mapData.Min) mapData.Min = heightValue;

                mapData.Data[x, y] = heightValue;
            }
        }
    }

    // Build a Tile array from our data
    private void LoadTiles()
    {
        Tiles = new Tile[Width, Height];

        for(var x = 0; x < Width; x++)
        {
            for(var y = 0; y < Height; y++)
            {
                Tile t = new Tile();
                t.X = x;
                t.Y = y;

                //set heightmap value
                float heightValue = HeightData.Data[x, y];
                heightValue = (heightValue - HeightData.Min) / (HeightData.Max - HeightData.Min);
                t.HeightValue = heightValue;


                if (heightValue < DeepWater)
                {
                    t.HeightType = HeightType.DeepWater;
                    t.Collidable = false;
                }
                else if (heightValue < ShallowWater)
                {
                    t.HeightType = HeightType.ShallowWater;
                    t.Collidable = false;
                }
                else if (heightValue < Sand)
                {
                    t.HeightType = HeightType.Sand;
                    t.Collidable = true;
                }
                else if (heightValue < Grass)
                {
                    t.HeightType = HeightType.Grass;
                    t.Collidable = true;
                }
                else if (heightValue < Forest)
                {
                    t.HeightType = HeightType.Forest;
                    t.Collidable = true;
                }
                else if (heightValue < Rock)
                {
                    t.HeightType = HeightType.Rock;
                    t.Collidable = true;
                }
                else
                {
                    t.HeightType = HeightType.Snow;
                    t.Collidable = true;
                }


                //set heat type
                //if (heatValue < ColdestValue)
                //    t.HeatType = HeatType.Coldest;
                //else if (heatValue < ColderValue)
                //    t.HeatType = HeatType.Colder;
                //else if (heatValue < ColdValue)
                //    t.HeatType = HeatType.Cold;
                //else if (heatValue < WarmValue)
                //    t.HeatType = HeatType.Warm;
                //else if (heatValue < WarmerValue)
                //    t.HeatType = HeatType.Warmer;
                //else
                //    t.HeatType = HeatType.Warmest;

                Tiles[x, y] = t;
            }
        }

        UpdateNeighbors();

        UpdateBitmasks();

        
    }

    private void UpdateBitmasks()
    {
        for(var x = 0; x < Width; x++)
        {
            for(var y = 0; y < Height; y++)
            {
                Tiles[x, y].UpdateBitmask();
            }
        }
    }


    private void FloodFill()
    {
        // Use a stack instead of recursion
        Stack<Tile> stack = new Stack<Tile>();

        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {

                Tile t = Tiles[x, y];

                //Tile already flood filled, skip
                if (t.FloodFilled) continue;

                // Land
                if (t.Collidable)
                {
                    TileGroup group = new TileGroup();
                    group.Type = TileGroupType.Land;
                    stack.Push(t);

                    while (stack.Count > 0)
                    {
                        FloodFill(stack.Pop(), ref group, ref stack);
                    }

                    if (group.Tiles.Count > 0)
                        Lands.Add(group);
                }
                // Water
                else
                {
                    TileGroup group = new TileGroup();
                    group.Type = TileGroupType.Water;
                    stack.Push(t);

                    while (stack.Count > 0)
                    {
                        FloodFill(stack.Pop(), ref group, ref stack);
                    }

                    if (group.Tiles.Count > 0)
                        Waters.Add(group);
                }
            }
        }
    }


    private void FloodFill(Tile tile, ref TileGroup tiles, ref Stack<Tile> stack)
    {
        // Validate
        if (tile.FloodFilled)
            return;
        if (tiles.Type == TileGroupType.Land && !tile.Collidable)
            return;
        if (tiles.Type == TileGroupType.Water && tile.Collidable)
            return;

        // Add to TileGroup
        tiles.Tiles.Add(tile);
        tile.FloodFilled = true;

        // floodfill into neighbors
        Tile t = GetTop(tile);
        if (!t.FloodFilled && tile.Collidable == t.Collidable)
            stack.Push(t);
        t = GetBottom(tile);
        if (!t.FloodFilled && tile.Collidable == t.Collidable)
            stack.Push(t);
        t = GetLeft(tile);
        if (!t.FloodFilled && tile.Collidable == t.Collidable)
            stack.Push(t);
        t = GetRight(tile);
        if (!t.FloodFilled && tile.Collidable == t.Collidable)
            stack.Push(t);
    }

    private Tile GetTop(Tile t)
    {
        return Tiles[t.X, MathHelper.Mod(t.Y - 1, Height)];
    }
    private Tile GetBottom(Tile t)
    {
        return Tiles[t.X, MathHelper.Mod(t.Y + 1, Height)];
    }
    private Tile GetLeft(Tile t)
    {
        return Tiles[MathHelper.Mod(t.X - 1, Width), t.Y];
    }
    private Tile GetRight(Tile t)
    {
        return Tiles[MathHelper.Mod(t.X + 1, Width), t.Y];
    }


    private void UpdateNeighbors()
    {
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                Tile t = Tiles[x, y];

                t.Top = GetTop(t);
                t.Bottom = GetBottom(t);
                t.Left = GetLeft(t);
                t.Right = GetRight(t);
            }
        }
    }


    private void DigRiverGroups()
    {
        for (int i = 0; i < RiverGroups.Count; i++)
        {

            RiverGroup group = RiverGroups[i];
            River longest = null;

            //Find longest river in this group
            for (int j = 0; j < group.Rivers.Count; j++)
            {
                River river = group.Rivers[j];
                if (longest == null)
                    longest = river;
                else if (longest.Tiles.Count < river.Tiles.Count)
                    longest = river;
            }

            if (longest != null)
            {
                //Dig out longest path first
                DigRiver(longest);

                for (int j = 0; j < group.Rivers.Count; j++)
                {
                    River river = group.Rivers[j];
                    if (river != longest)
                    {
                        DigRiver(river, longest);
                    }
                }
            }
        }
    }

    // Dig river based on a parent river vein
    private void DigRiver(River river, River parent)
    {
        int intersectionID = 0;
        int intersectionSize = 0;

        // determine point of intersection
        for (int i = 0; i < river.Tiles.Count; i++)
        {
            Tile t1 = river.Tiles[i];
            for (int j = 0; j < parent.Tiles.Count; j++)
            {
                Tile t2 = parent.Tiles[j];
                if (t1 == t2)
                {
                    intersectionID = i;
                    intersectionSize = t2.RiverSize;
                }
            }
        }

        int counter = 0;
        int intersectionCount = river.Tiles.Count - intersectionID;
        int size = UnityEngine.Random.Range(intersectionSize, 5);
        river.Length = river.Tiles.Count;

        // randomize size change
        int two = river.Length / 2;
        int three = two / 2;
        int four = three / 2;
        int five = four / 2;

        int twomin = two / 3;
        int threemin = three / 3;
        int fourmin = four / 3;
        int fivemin = five / 3;

        // randomize length of each size
        int count1 = UnityEngine.Random.Range(fivemin, five);
        if (size < 4)
        {
            count1 = 0;
        }
        int count2 = count1 + UnityEngine.Random.Range(fourmin, four);
        if (size < 3)
        {
            count2 = 0;
            count1 = 0;
        }
        int count3 = count2 + UnityEngine.Random.Range(threemin, three);
        if (size < 2)
        {
            count3 = 0;
            count2 = 0;
            count1 = 0;
        }
        int count4 = count3 + UnityEngine.Random.Range(twomin, two);

        // Make sure we are not digging past the river path
        if (count4 > river.Length)
        {
            int extra = count4 - river.Length;
            while (extra > 0)
            {
                if (count1 > 0) { count1--; count2--; count3--; count4--; extra--; }
                else if (count2 > 0) { count2--; count3--; count4--; extra--; }
                else if (count3 > 0) { count3--; count4--; extra--; }
                else if (count4 > 0) { count4--; extra--; }
            }
        }

        // adjust size of river at intersection point
        if (intersectionSize == 1)
        {
            count4 = intersectionCount;
            count1 = 0;
            count2 = 0;
            count3 = 0;
        }
        else if (intersectionSize == 2)
        {
            count3 = intersectionCount;
            count1 = 0;
            count2 = 0;
        }
        else if (intersectionSize == 3)
        {
            count2 = intersectionCount;
            count1 = 0;
        }
        else if (intersectionSize == 4)
        {
            count1 = intersectionCount;
        }
        else
        {
            count1 = 0;
            count2 = 0;
            count3 = 0;
            count4 = 0;
        }

        // dig out the river
        for (int i = river.Tiles.Count - 1; i >= 0; i--)
        {

            Tile t = river.Tiles[i];

            if (counter < count1)
            {
                t.DigRiver(river, 4);
            }
            else if (counter < count2)
            {
                t.DigRiver(river, 3);
            }
            else if (counter < count3)
            {
                t.DigRiver(river, 2);
            }
            else if (counter < count4)
            {
                t.DigRiver(river, 1);
            }
            else
            {
                t.DigRiver(river, 0);
            }
            counter++;
        }
    }

    // Dig river
    private void DigRiver(River river)
    {
        int counter = 0;

        // How wide are we digging this river?
        int size = UnityEngine.Random.Range(1, 5);
        river.Length = river.Tiles.Count;

        // randomize size change
        int two = river.Length / 2;
        int three = two / 2;
        int four = three / 2;
        int five = four / 2;

        int twomin = two / 3;
        int threemin = three / 3;
        int fourmin = four / 3;
        int fivemin = five / 3;

        // randomize lenght of each size
        int count1 = UnityEngine.Random.Range(fivemin, five);
        if (size < 4)
        {
            count1 = 0;
        }
        int count2 = count1 + UnityEngine.Random.Range(fourmin, four);
        if (size < 3)
        {
            count2 = 0;
            count1 = 0;
        }
        int count3 = count2 + UnityEngine.Random.Range(threemin, three);
        if (size < 2)
        {
            count3 = 0;
            count2 = 0;
            count1 = 0;
        }
        int count4 = count3 + UnityEngine.Random.Range(twomin, two);

        // Make sure we are not digging past the river path
        if (count4 > river.Length)
        {
            int extra = count4 - river.Length;
            while (extra > 0)
            {
                if (count1 > 0) { count1--; count2--; count3--; count4--; extra--; }
                else if (count2 > 0) { count2--; count3--; count4--; extra--; }
                else if (count3 > 0) { count3--; count4--; extra--; }
                else if (count4 > 0) { count4--; extra--; }
            }
        }

        // Dig it out
        for (int i = river.Tiles.Count - 1; i >= 0; i--)
        {
            Tile t = river.Tiles[i];

            if (counter < count1)
            {
                t.DigRiver(river, 4);
            }
            else if (counter < count2)
            {
                t.DigRiver(river, 3);
            }
            else if (counter < count3)
            {
                t.DigRiver(river, 2);
            }
            else if (counter < count4)
            {
                t.DigRiver(river, 1);
            }
            else
            {
                t.DigRiver(river, 0);
            }
            counter++;
        }
    }


    private void BuildRiverGroups()
    {
        //loop each tile, checking if it belongs to multiple rivers
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                Tile t = Tiles[x, y];

                if (t.Rivers.Count > 1)
                {
                    // multiple rivers == intersection
                    RiverGroup group = null;

                    // Does a rivergroup already exist for this group?
                    for (int n = 0; n < t.Rivers.Count; n++)
                    {
                        River tileriver = t.Rivers[n];
                        for (int i = 0; i < RiverGroups.Count; i++)
                        {
                            for (int j = 0; j < RiverGroups[i].Rivers.Count; j++)
                            {
                                River river = RiverGroups[i].Rivers[j];
                                if (river.ID == tileriver.ID)
                                {
                                    group = RiverGroups[i];
                                }
                                if (group != null) break;
                            }
                            if (group != null) break;
                        }
                        if (group != null) break;
                    }

                    // existing group found -- add to it
                    if (group != null)
                    {
                        for (int n = 0; n < t.Rivers.Count; n++)
                        {
                            if (!group.Rivers.Contains(t.Rivers[n]))
                                group.Rivers.Add(t.Rivers[n]);
                        }
                    }
                    else   //No existing group found - create a new one
                    {
                        group = new RiverGroup();
                        for (int n = 0; n < t.Rivers.Count; n++)
                        {
                            group.Rivers.Add(t.Rivers[n]);
                        }
                        RiverGroups.Add(group);
                    }
                }
            }
        }
    }


    private void GenerateRivers()
    {
        int attempts = 0;
        int rivercount = RiverCount;
        Rivers = new List<River>();

        // Generate some rivers
        while (rivercount > 0 && attempts < MaxRiverAttempts)
        {

            // Get a random tile
            int x = UnityEngine.Random.Range(0, Width);
            int y = UnityEngine.Random.Range(0, Height);
            Tile tile = Tiles[x, y];

            // validate the tile
            if (!tile.Collidable) continue;
            if (tile.Rivers.Count > 0) continue;

            if (tile.HeightValue > MinRiverHeight)
            {
                // Tile is good to start river from
                River river = new River(rivercount);

                // Figure out the direction this river will try to flow
                river.CurrentDirection = tile.GetLowestNeighbor();

                // Recursively find a path to water
                FindPathToWater(tile, river.CurrentDirection, ref river);

                // Validate the generated river 
                if (river.TurnCount < MinRiverTurns || river.Tiles.Count < MinRiverLength || river.Intersections > MaxRiverIntersections)
                {
                    //Validation failed - remove this river
                    for (int i = 0; i < river.Tiles.Count; i++)
                    {
                        Tile t = river.Tiles[i];
                        t.Rivers.Remove(river);
                    }
                }
                else if (river.Tiles.Count >= MinRiverLength)
                {
                    //Validation passed - Add river to list
                    Rivers.Add(river);
                    tile.Rivers.Add(river);
                    rivercount--;
                }
            }
            attempts++;
        }
    }


    private void FindPathToWater(Tile tile, Direction direction, ref River river)
    {
        if (tile.Rivers.Contains(river))
            return;

        // check if there is already a river on this tile
        if (tile.Rivers.Count > 0)
            river.Intersections++;

        river.AddTile(tile);

        // get neighbors
        Tile left = GetLeft(tile);
        Tile right = GetRight(tile);
        Tile top = GetTop(tile);
        Tile bottom = GetBottom(tile);

        float leftValue = int.MaxValue;
        float rightValue = int.MaxValue;
        float topValue = int.MaxValue;
        float bottomValue = int.MaxValue;

        // query height values of neighbors
        if (left.GetRiverNeighborCount(river) < 2 && !river.Tiles.Contains(left))
            leftValue = left.HeightValue;
        if (right.GetRiverNeighborCount(river) < 2 && !river.Tiles.Contains(right))
            rightValue = right.HeightValue;
        if (top.GetRiverNeighborCount(river) < 2 && !river.Tiles.Contains(top))
            topValue = top.HeightValue;
        if (bottom.GetRiverNeighborCount(river) < 2 && !river.Tiles.Contains(bottom))
            bottomValue = bottom.HeightValue;

        // if neighbor is existing river that is not this one, flow into it
        if (bottom.Rivers.Count == 0 && !bottom.Collidable)
            bottomValue = 0;
        if (top.Rivers.Count == 0 && !top.Collidable)
            topValue = 0;
        if (left.Rivers.Count == 0 && !left.Collidable)
            leftValue = 0;
        if (right.Rivers.Count == 0 && !right.Collidable)
            rightValue = 0;

        // override flow direction if a tile is significantly lower
        if (direction == Direction.Left)
            if (Mathf.Abs(rightValue - leftValue) < 0.1f)
                rightValue = int.MaxValue;
        if (direction == Direction.Right)
            if (Mathf.Abs(rightValue - leftValue) < 0.1f)
                leftValue = int.MaxValue;
        if (direction == Direction.Top)
            if (Mathf.Abs(topValue - bottomValue) < 0.1f)
                bottomValue = int.MaxValue;
        if (direction == Direction.Bottom)
            if (Mathf.Abs(topValue - bottomValue) < 0.1f)
                topValue = int.MaxValue;

        // find mininum
        float min = Mathf.Min(Mathf.Min(Mathf.Min(leftValue, rightValue), topValue), bottomValue);

        // if no minimum found - exit
        if (min == int.MaxValue)
            return;

        //Move to next neighbor
        if (min == leftValue)
        {
            if (left.Collidable)
            {
                if (river.CurrentDirection != Direction.Left)
                {
                    river.TurnCount++;
                    river.CurrentDirection = Direction.Left;
                }
                FindPathToWater(left, direction, ref river);
            }
        }
        else if (min == rightValue)
        {
            if (right.Collidable)
            {
                if (river.CurrentDirection != Direction.Right)
                {
                    river.TurnCount++;
                    river.CurrentDirection = Direction.Right;
                }
                FindPathToWater(right, direction, ref river);
            }
        }
        else if (min == bottomValue)
        {
            if (bottom.Collidable)
            {
                if (river.CurrentDirection != Direction.Bottom)
                {
                    river.TurnCount++;
                    river.CurrentDirection = Direction.Bottom;
                }
                FindPathToWater(bottom, direction, ref river);
            }
        }
        else if (min == topValue)
        {
            if (top.Collidable)
            {
                if (river.CurrentDirection != Direction.Top)
                {
                    river.TurnCount++;
                    river.CurrentDirection = Direction.Top;
                }
                FindPathToWater(top, direction, ref river);
            }
        }
    }
}
