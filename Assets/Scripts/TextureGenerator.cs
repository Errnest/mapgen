﻿using UnityEngine;
using System.Collections.Generic;

public static class TextureGenerator
{

    // Height Map Colors
    private static Color DeepColor = new Color(15 / 255f, 30 / 255f, 80 / 255f, 1);
    private static Color ShallowColor = new Color(15 / 255f, 40 / 255f, 90 / 255f, 1);
    private static Color RiverColor = new Color(30 / 255f, 120 / 255f, 200 / 255f, 1);
    private static Color SandColor = new Color(198 / 255f, 190 / 255f, 31 / 255f, 1);
    private static Color GrassColor = new Color(50 / 255f, 220 / 255f, 20 / 255f, 1);
    private static Color ForestColor = new Color(16 / 255f, 160 / 255f, 0, 1);
    private static Color RockColor = new Color(0.5f, 0.5f, 0.5f, 1);
    private static Color SnowColor = new Color(1, 1, 1, 1);

    private static Color IceWater = new Color(210 / 255f, 255 / 255f, 252 / 255f, 1);
    private static Color ColdWater = new Color(119 / 255f, 156 / 255f, 213 / 255f, 1);
    private static Color RiverWater = new Color(65 / 255f, 110 / 255f, 179 / 255f, 1);


    public static Texture2D GetNoiseMapTexture(int width, int height, Tile[,] tiles)
    {
        var texture = new Texture2D(width, height);
        var pixels = new Color[width * height];

        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                float value = tiles[x, y].HeightValue;

                //Set color range, 0 = black, 1 = white
                pixels[x + y * width] = Color.Lerp(Color.black, Color.white, value);
            }
        }

        texture.SetPixels(pixels);
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.Apply();
        return texture;
    }

    public static Texture2D GetHeightMapTexture(int width, int height, Tile[,] tiles)
    {
        var texture = new Texture2D(width, height);
        var pixels = new Color[width * height];

        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                switch (tiles[x, y].HeightType)
                {
                    case HeightType.DeepWater:
                        pixels[x + y * width] = DeepColor;
                        break;
                    case HeightType.ShallowWater:
                        pixels[x + y * width] = ShallowColor;
                        break;
                    case HeightType.Sand:
                        pixels[x + y * width] = SandColor;
                        break;
                    case HeightType.Grass:
                        pixels[x + y * width] = GrassColor;
                        break;
                    case HeightType.Forest:
                        pixels[x + y * width] = ForestColor;
                        break;
                    case HeightType.Rock:
                        pixels[x + y * width] = RockColor;
                        break;
                    case HeightType.Snow:
                        pixels[x + y * width] = SnowColor;
                        break;
                    case HeightType.River:
                        pixels[x + y * width] = RiverColor;
                        break;
                }

                //darken the color if a edge tile
                if ((int)tiles[x, y].HeightType > 2 && tiles[x, y].Bitmask != 15)
                    pixels[x + y * width] = Color.Lerp(pixels[x + y * width], Color.black, 0.4f);

            }
        }

        texture.SetPixels(pixels);
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.Apply();
        return texture;
    }

    public static Texture2D GetWaterLandMapTexture(int width, int height, Tile[,] tiles)
    {
        var texture = new Texture2D(width, height);
        var pixels = new Color[width * height];

        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                float value = tiles[x, y].HeightValue;

                
                if(value <= 0.4f)
                {
                    pixels[x + y * width] = Color.blue * 0.7f;
                }
                else
                {
                    pixels[x + y * width] = Color.white;
                }
            }
        }

        texture.SetPixels(pixels);
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.Apply();
        return texture;
    }
}
